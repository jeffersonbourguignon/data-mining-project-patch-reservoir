from numpy import where
import pandas as pd
#import matplotlib.pyplot as plt
from pandas.core.indexes.base import Index
import csv

#extractFile
file ='tryMe.csv'
csv = pd.read_csv(file)
df = csv

# to drop a column
# df = df.iloc[: , 1:]

#drop index column
df.reset_index(drop=True, inplace=True)

newDf = df.drop(['GlobalID', 'EditDate', 'Nitrates', 'Phosphorus'], axis=1)
print(newDf.info())

print(df['Conductivi'] != 0)
count = (df['Phosphorus'] > 0)
count = count.count()
print('Phosphorus count: ', where(df['Phosphorus'] > 0))
print('Phosphorus count: ', where(df['Phosphorus'] > 0))
newDf = newDf.loc[newDf['DO'] < 12]
newDf = newDf.loc[newDf['DO'] > 0]
newDf = newDf.loc[newDf['pH'] != 106.2]
newDf = newDf.loc[newDf['pH'] != 0]
print(newDf)
newDf.to_csv('fixedDO.csv')
print(newDf['pH'].max())

data = {'class': ['index','Temperature_C,pH,DO','Depth_Ft','Secchi_Depth','Conductivi','CreationDa','Latitude','Longitude']}
newDf.insert(data)

newDf.to_csv('withClass.csv')

# print(df.info())
# df.to_csv('fullData.csv')

# noZeroes = (df != 0).any(axis=1)
# print(noZeroes)
# dfNoZeroes = df.loc[noZeroes]
# print(dfNoZeroes)
import pandas as pd
import matplotlib.pyplot as plt
from pandas.core.indexes.base import Index


file ='tryMe.csv'
csv = pd.read_csv(file)
df = csv

# Drop first column of dataframe
df = df.iloc[: , 1:]
df = df[['pH', 'DO', 'Depth_Ft']]
print(df.info())
df1 = csv
# print(df1.head())
# print(df1.info())
# print(min(df1['Temperature_C']))
# df2 = df1[~(df1 == 0).all(axis=1)] #drop all rows with zero
# print(df2.head())
# print(df2.info())
# print(df1['pH'])

print(df1.info())
#removing zeroes from pH
print('before')
print('pH = 0 ', (df1['pH'] == 0).sum())
print('pH > 0 ', (df1['pH'] > 0).sum(), '\n')
df2 = df1.loc[df1['pH'] > 0]
print('after')
print('pH = 0 ', (df2['pH'] == 0).sum())
print('pH > 0 ', (df2['pH'] > 0).sum(), '\n')

#removing zeroes from DO
print('before')
print('DO = 0 ', (df2['DO'] == 0).sum())
print('DO > 0 ', (df2['DO'] > 0).sum(), '\n')
df2 = df2.loc[df1['DO'] > 0]
print('after')
print('DO = 0 ', (df2['DO'] == 0).sum())
print('DO > 0 ', (df2['DO'] > 0).sum(), '\n')



plt.show()

max = df2['DO'].max()
print(df2['DO'].max())
df2[df2['DO'] == max] = df2['DO'].median()
print(df2['DO'].max())

ylab = list(range(0, 102))
plt.scatter(df2['DO'], ylab)
plt.savefig('plot.png', dpi=300, bbox_inches='tight')

# print(df2.head())
# print(sorted(df2['Temperature_C']))
# df3 = df2.loc[df2['DO'] > .1]
# print(df3.head())
# print(df3.info())


df2.to_csv('test1weka.csv')

#big thing to remember add a string before the comma on the csv file or it will not open on weka 
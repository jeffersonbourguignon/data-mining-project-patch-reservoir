import pandas as pd

file = 'MassDEP Water Quality Monitoring Stations/GISDATA.DWMWPP_WATER_QUALITY_STATIONS.csv'
df = pd.read_csv(file)


df1 = df.drop(['gdb_geomattr_data', 'shape', 'latitude', 'longitude','FID'], axis=1)
print(df1.info())
df.to_csv('MassDEPWaterQualityMonitoringStations.csv')
#print(df['surveytype', ])
#df1 = df['surveytype']
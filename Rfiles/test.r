# Load aplpack to make the bagplot() function available
library(aplpack)
library(corrplot)
library(ggplot2)


# Create a bagplot for the same two variables
#bagplot(Cars93$Min.Price, Cars93$Max.Price, cex = 1.2)
test <- fixedDO
# Extract the numerical variables from UScereal
head(test)

numericalvars <- test[,2:7]

# Compute the correlation matrix for these variables

corrMat <- cor(numericalvars)

# Generate the correlation ellipse plot
corrplot(corrMat, method = "ellipse")

basePlot <- ggplot(fixedDO, aes(x = DO, y = Temperature_C))

# Display the basic scatterplot
basePlot + geom_point()


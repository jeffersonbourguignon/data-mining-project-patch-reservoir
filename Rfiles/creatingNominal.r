library(ggplot2)
library(magrittr)#forward piping %>%
library(dplyr)


month <- months(by_date$EditDate)
by_date$monthNominal <- month
plot(by_date)

by_date
write.csv(by_date, file = "nominalDate.csv")
attributes(by_date)


fixedCoordinate <- paste(by_date$Latitude, by_date$Longitude)
by_date$coordinate <- fixedCoordinate
by_date

#concatemating the coordinate to use as nominal in one column
unwantedColumns <- c('Latitude', 'Longitude', 'cleanedDf$EditDate')
by_date = by_date[!(names(by_date)) %in% unwantedColumns]
by_date
write.csv(by_date, file = "coordinateAsNominal.csv")

#try making a progression on temperature

# Compute groupwise measures of spread
by_date %>%
  group_by(monthNominal) %>%
  summarize(sd(Temperature_C),
            IQR(Temperature_C),
            n())

by_date %>%
  ggplot(aes(x = Temperature_C, fill = monthNominal)) + 
  geom_density(alpha = .3)

#look for modality use logs normalize if necessary use mutate to normalize use filter to remove outliers
library(magrittr)#forward piping %>%
library(dplyr)#for grouping
library(RColorBrewer)

#df1 <- df %>% group_by(EditDate, Longitude, Latitude)
#do not use Phosphorus, Nitrates, depth_ft, Sechi_Depth
#use conductivity seperately try generating values

month <- months(by_date$EditDate)
barplot(months)


var <- aggregate(cleanedDf[''], by=list(cleanedDf$EditDate), FUN = sum)
plot(var)

#playig with group by
by_date <- cleanedDf %>% group_by(cleanedDf$EditDate)
year2017 <- cleanedDf[year(cleanedDf$EditDate) == 2017, ]
plot(year2017$EditDate, year2017$Temperature_C)
tail(by_date)
by_date$EditDate
year(year2017$EditDate)
year(cleanedDf$EditDate)
attributes(cleanedDf)

month(by_date$EditDate)

ggplot(cleanedDf, aes(x = EditDate, y = Temperature_C, color = Temperature_C, size = 3, shape = factor(month(EditDate)), group = month(EditDate))) +
  geom_point(colors = cleanedDf$Temperature_C) + scale_colour_gradient(low = "yellow", high = "red", na.value = NA)

month(cleanedDf$EditDate)

round(year2017$Temperature_C, 0)
year2017$EditDate

###makes no sense for this maybe after using the small cleaned do and ph

tab <- table(cleanedDf$Temperature_C, cleanedDf$EditDate)
options(scipen = 999, digits = 3) # Print fewer digits
prop.table(tab)     # Joint proportions
prop.table(tab, 2)  # Conditional on columns



###just playing
print(summary(cleanedDf$pH))
smalldf <- cleanedDf[!(cleanedDf$pH == 0 | cleanedDf$pH > 106), ]

nrow(smalldf)
which(smalldf$pH == 0)
cleanedDf <- cleanedDf[!(cleanedDf$DO == 0), ]
nrow(cleanedDf)
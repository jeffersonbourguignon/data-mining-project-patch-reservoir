library(ggplot2)
library(lubridate)

df <- read.csv('fullData.csv')

#removing unwanted columns
removeUnwantedColumns <- c('index', 'Phosphorus', 'Nitrates', 'Depth_Ft', 'Secchi_Depth', 'Conductivi', 'CreationDa', 'GlobalID')
cleanedDf = df[!(names(df)) %in% removeUnwantedColumns]
head(cleanedDf)



#saving data with removed unwanted columns
write.csv(cleanedDf, file = "removedUnwantedColumns.csv")

#checking excess values in temp
print(summary(cleanedDf$Temperature_C))
#checking how many excess values in temp
print(which(cleanedDf$Temperature_C > 100))
print(which(cleanedDf$Temperature_C == 0))
print(which(cleanedDf$Temperature_C < 1))
#eliminating rows values over 100 or less than .01 in temperature
cleanedDf <- cleanedDf[!(cleanedDf$Temperature_C > 100 | cleanedDf$Temperature_C < 0.1), ]
print(summary(cleanedDf$Temperature_C))

#saving data with removed unwanted values in temperature
write.csv(cleanedDf, file = "temperatureOnlyGoodValues.csv")

#***before cleaning temperature*** checking dissolved oxygen values
print(summary(df$DO))
#cleaning dissolved oxygen, expert advised values over 12 are bad  
df1 <- df[!(df$DO > 12), ]
#checking amount of data left
nrow(df1)
#checking number of values that are zero, expert advised zero values are empty entries
which(df1$DO == 0)
#cleaning empty entries and checking how much data is left
df1 <- df1[!(df1$DO == 0), ]
nrow(df1)

#saving data with removed unwanted values in dissolved oxygen
write.csv(df1, file = "dissolvedOxygenOnlyGoodValues.csv")

#checking dissolved oxygen values
print(summary(cleanedDf$DO))
#cleaning dissolved oxygen, expert advised values over 12 are bad  
cleanedDf <- cleanedDf[!(cleanedDf$DO > 12), ]
#checking amount of data left
nrow(cleanedDf)
#checking number of values that are zero, expert advised zero values are empty entries
which(cleanedDf$DO == 0)
#cleaning empty entries and checking how much data is left
cleanedDf <- cleanedDf[!(cleanedDf$DO == 0), ]
nrow(cleanedDf)

#checking all important values for odd values
plot(cleanedDf$DO)
plot(cleanedDf$Temperature_C)
plot(cleanedDf$pH)

#cleaning pH only
df2 <- df[!(df$pH == 0 | df$pH > 40), ]
summary(df2)
plot(df2$pH)
nrow(df2)#worth looking at it in this format
write.csv(df2, file = "pHOnlyGoodValues.csv")

#cleaning all data from extraneous values
noZeroesDF <- cleanedDf[!(cleanedDf$pH == 0 | cleanedDf$pH > 40), ]
summary(noZeroesDF)
plot(noZeroesDF$pH)
nrow(noZeroesDF)#worth looking at but very small, mention that
write.csv(noZeroesDF, file = "noExtraneousValues.csv")

###############################^
#before fixing the date format#|
###############################|

###############################|
#after fixing the date format #|
###############################v

#fixing the dates format
print(noZeroesDF$EditDate)
noZeroesDF$EditDate <- as.Date(noZeroesDF$EditDate, "%m/%d/%Y")
print(noZeroesDF$EditDate)
write.csv(noZeroesDF, file = "correctDateFormatnoExtraneousValues.csv")